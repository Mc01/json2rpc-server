import socket, ssl
from json import dumps, loads

#------EXAMPLES------#
add_user = {'method':'v-add-user', 'args':['test1', 'tester','test@test1.com']}
delete_user =  {'method':'v-delete-user', 'args':['test1']}
suspend_user = {'method':'v-suspend-user', 'args':['test1']}
unsuspend_user =  {'method':'v-unsuspend-user', 'args':['test1']}
#------EXAMPLES------#

#------CONFIG------#
ip = '127.0.0.1'
port = 8092
crt = 'cert/server.crt'
sslv = ssl.PROTOCOL_TLSv1
data = delete_user
#------CONFIG------#

print 'Client init.'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssls = ssl.wrap_socket(s, ca_certs = crt, cert_reqs = ssl.CERT_REQUIRED, ssl_version = sslv)
ssls.connect((ip, port))
ssls.send(dumps(data))
response = loads(ssls.recv(10240))
print response['callback']
ssls.close()
