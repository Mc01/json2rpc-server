import SocketServer
import socket, ssl
from json import loads, dumps
import os, glob, time

#------CONFIG------#
ip = '127.0.0.1'
port = 8092
crt = 'cert/server.crt'
key = 'cert/server.key'
sslv = ssl.PROTOCOL_TLSv1
allowed_list = [ip, '192.168.0.1', '192.168.0.2']
logfile = 'log/log.txt'
path = '/usr/local/vesta/bin/'
#------CONFIG------#

class App(SocketServer.ThreadingTCPServer):
        def __init__(self, address, RequestHandlerClass, bind_and_activate = Tru$
                SocketServer.TCPServer.__init__(self, address, RequestHandlerCla$
                allow_reuse_address = True
                print 'Server init.'
        def get_request(self):
                newsocket, fromaddr = self.socket.accept()
                stream = ssl.wrap_socket(newsocket, server_side = True, certfile = crt, keyfile = key, ssl_version = sslv)
                global client
                client = str(fromaddr[0])
                with open(logfile, 'a') as o:
                        o.write(time.strftime("%c") + ' ' + client + '\n')
                return stream, fromaddr

class AppHandler(SocketServer.BaseRequestHandler):
        def handle(self):
                try:
                        global client
                        if client in allowed_list:
                                data = loads(self.request.recv(1024).strip())
                                with open(logfile, 'a') as o:
                                        o.write(str(data['method']) + ' ' + str(data['args']) + '\n')
                                global names
                                if data['method'] in names:
                                        response = self.process(data)
                                else:
                                        response = {'callback':'Method not found.'}
                                self.request.sendall(dumps(response))
                except Exception, e:
                        print 'Something blown up: ', e
        def process(self, data):
                try:
                        print 'Method: ', data['method']
                        print 'Args: ', data['args']
                        length = len(data['args'])
                        f_args = self.fetch_args(data['method'])
                        argc = int(f_args['argc'])
                        opts = int(f_args['opts'])
                        if argc >= length and (argc - opts) <= length:
                                argv = ' '.join(data['args'])
                                argv = ''
                                for w in data['args']:
                                        w = self.clean_arg(w)
                                        argv = argv + ' ' + w
                                argv = argv.rstrip(' ')
                                cmd = path + data['method'] + ' ' + argv
                                callback = os.popen(cmd).read().rstrip('\n')
                        else:
                                callback = 'Invalid arguments.'
                        response = {'callback':callback}
                        return response
                except ValueError:
                        raise ValueError('Bad JSON')
        def fetch_args(self, method):
                argc = 0
                opts = 0
                cmd = "awk '/ions:/' " + path + method
                callback = os.popen(cmd).read()
                if callback == '':
                        cmd = "awk '/check_args/' " + path + method
                        callback = os.popen(cmd).read()
                        if callback != '':
                                numb = []
                                for c in callback:
                                        if c.isdigit():
                                                numb += c
                                argc = max(numb)
                elif callback.find('NONE') == -1:
                        callback = callback.lstrip('#')
                        words = callback.split()
                        opts = 0
                        for w in words:
                                if w.find('[') != -1:
                                        opts = opts + 1
                        argc = len(words) -1
                print 'Argc: ', argc, 'Opts: ', opts
                return {'argc':argc, 'opts':opts}
        def clean_arg(self, arg):
                arg = ' ' + arg
                if arg.find('|'):
                        arg = arg.replace('|', '')
                if arg.find('>'):
                        arg = arg.replace('>', '')
                if arg.find('<'):
                        arg = arg.replace('<', '')
                if arg.find('['):
                        arg = arg.replace('[', '')
                if arg.find(']'):
                        arg = arg.replace(']', '')
                if arg.find('('):
                        arg = arg.replace('(', '')
                if arg.find(')'):
                        arg = arg.replace(')', '')
                arg = arg.lstrip(' ')
                return arg

def fetch_names():
        print 'Fetching names.'
        prefix = glob.glob(path + '*')
        names = []
        client = ''
        for n in prefix:
                n = n[len(path):]
                names.append(n)
        return names

names = fetch_names()
print 'Names fetched.'
app = App((ip, port), AppHandler)
try:
        app.serve_forever()
except KeyboardInterrupt:
        print 'Server exit.'
